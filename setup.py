#!/usr/bin/env python3
# windows-like program installer
import os, sys, random, os.path, shutil, time, platform
try:
    from PyQt5.QtCore import *
    from PyQt5.QtGui import *
    from PyQt5.QtWidgets import *
except:
    os.system("pip install PyQt5")
    from PyQt5.QtCore import *
    from PyQt5.QtGui import *
    from PyQt5.QtWidgets import *

HERE = os.path.dirname(os.path.realpath(__file__))

def getSetupInfo():
    global prgName
    global prgVer
    global prgCnam
    global prgDest
    global prgPlatform
    global prgArch
    global usdOS
    global mainFile
    
    prgName = open(f"{HERE}/prg_info/prog", "r").read()
    prgVer = open(f"{HERE}/prg_info/ver", "r").read()
    prgCnam = open(f"{HERE}/prg_info/cnam", "r").read()
    prgDest = open(f"{HERE}/prg_info/dest", "r").read()
    prgPlatform = open(f"{HERE}/prg_info/platform", "r").read()
    prgArch = open(f"{HERE}/prg_info/arch", "r").read()
    mainFile = open(f"{HERE}/prg_info/mex", "r").read()
    
    usdOS = platform.system()

def install_program():
    pt2.hide()
    pt3.hide()
    nextB1.hide()
    if os.path.exists(f"{prgDestT}/{prgCnam}.desktop"):
        pt.setText("{prgName} is already installed")
    else:
        pt.setText("Installing...")
    
        # create directories
        print(f"Create {prgDestT}")
        os.system(f"mkdir -p {prgDestT}")
        print("Create $HOME/.local/bin")
        os.system(f"mkdir -p $HOME/.local/bin $HOME/.local/share/applications")
        
        # copy the program files
        os.system(f"cp -rfv {HERE}/prg/* {prgDestT}/")
        
        # link the program to ~/.local/bin
        os.system(f"ln -svf {prgDestT}/bin/{mainFile} $HOME/.local/bin/{mainFile}")
        
        # insert program dir in desktop entry
        os.system(f"gsed -i 's|%%PROGRAM_DIR%%|{prgDestT}|' {prgDestT}/{prgCnam}.desktop || sed -i 's|%%PROGRAM_DIR%%|{prgDestT}|' {prgDestT}/{prgCnam}.desktop")
        
        # copy the desktop entry
        os.system(f"ln -svf {prgDestT}/{prgCnam}.desktop $HOME/.local/share/applications/{prgCnam}.desktop")
        pt.setText("Installation successful. You may close the installer.")

def install_summary():
    pt.show()
    pt2.show()
    pt3.show()
    pt.setText("Install Summary:")
    pt2.setText("Accepted License")
    pt2.resize(500,15)
    pt2.move(110,25)
    pt3.setText(f"Dest dir: {prgDestT}")
    pt3.resize(500,15)
    pt3.move(110,40)
    nextB1.setText("Install")
    dstBox.hide()
    nextB1.clicked.connect(install_program)

def prgDestination():
    global prgDestT
    pt.setText(f"Select directory to install {prgName}")
    pt.move(110,10)
    licBox.hide()
    dstBox.show()
    dstBox.resize(350,20)
    dstBox.move(110,25)
    dstBox.setText(prgDest)
    prgDestT = dstBox.text()
    nextB1.setText("Next")
    nextB1.clicked.connect(install_summary)

def acceptLicense():
    pt.setText("License Agreement")
    pt.move(110,10)
    pt.resize(500,15)
    pt.show()
    licBox.move(110,25)
    licBox.resize(350,400)
    licBox.show()
    licBox.insertPlainText(open(f"{HERE}/prg_info/license", "r").read())
    nextB1.show()
    nextB1.setText("I accept")
    nextB1.clicked.connect(prgDestination)

def check_platform():
    nextB1.hide()
    fti.hide()
    wlText.hide()
    if not usdOS == prgPlatform:
        pt.show()
        pt.setText(f"Sorry, {prgName} is not supported on {usdOS}!")
        pt.move(110,10)
        pt.resize(500,20)
    else:
        acceptLicense()

def main():
    app = QApplication(sys.argv)
    
    global w
    global wlText
    global fti
    global nextB1
    global pt
    global licBox
    global dstBox
    global pt2
    global pt3
    w = QWidget()
    pt = QLabel(w)
    licBox = QPlainTextEdit(w)
    dstBox = QLineEdit(w)
    pt2 = QLabel(w)
    pt2.hide()
    pt3 = QLabel(w)
    pt3.hide()
    dstBox.hide()
    pt.hide()
    licBox.hide()
    w.setWindowTitle(f"{prgName} Setup")
    w.setGeometry(100,100,500,500)
    
    banner = QLabel(w)
    banner.setPixmap(QPixmap(f"{HERE}/prg_info/banner.png").scaledToWidth(100).scaledToHeight(500))
    banner.move(0,0)
    
    wlText = QLabel(w)
    wlText.setText(f"Welcome to the {prgName} setup program!")
    wlText.move(110,10)
    
    fti = QLabel(w)
    fti.setText("Follow the instructions and press Next")
    fti.move(110,50)
    
    nextB1 = QPushButton(w)
    nextB1.setText("Next")
    nextB1.resize(75,30)
    nextB1.move(425,470)
    nextB1.clicked.connect(check_platform)
    
    w.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    getSetupInfo()
    main()
