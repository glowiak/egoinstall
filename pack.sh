#!/bin/sh
HERE=$(readlink -f $(dirname $0))
APP_NAME=$(cat $HERE/prg_info/cnam)
APP_VER=$(cat $HERE/prg_info/ver)
APP_OS=$(cat $HERE/prg_info/platform)
OUT=$HERE/../$APP_NAME-${APP_VER}_$APP_OS.ego
echo "==> Creating archive $OUT"
rm -rf $OUT
cat >> $OUT << "EOF"
#!/bin/sh
# EGO self-extracting archive
BSCODE="
EOF
tar cf - . | base64 >> $OUT
echo '"' >> $OUT
cat >> $OUT << "EOF"
if ! command -v python3
then
	echo "Python3 is needed to run!"
	exit 1
fi
rm -rf /tmp/inst
mkdir -p /tmp/inst
echo "$BSCODE" | base64 -d | tar xf - -C /tmp/inst
(cd /tmp/inst && python3 setup.py)
rm -rf /tmp/inst
exit 0
EOF
chmod 775 $OUT
echo "Done"
