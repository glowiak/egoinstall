# egoinstall

A graphical user-friendly program installer for UNIX,

Like installshield or msiexec

### Dependiences

Only for end user: Python3 with PyQt5, base64

### Creating installers

Clone this repository, place your program's binaries in prg/bin directory, it's sources in prg/src, edit the data files in prg_info/ and modify the prg_info/banner.png, rename and edit the prg/hello.desktop to match cnam (console name) of the program, move the app's main executable to prg/bin/{cnam}.

Try if the installer works correct with ./setup.py. If so, then run ./pack.sh and get executable .ego file as output in the parent directory.

The default is a Hello World program written in FreeBASIC.

### Roadmap

[DONE] installers

[WIP] make desktop files work

[WIP] uninstall program
